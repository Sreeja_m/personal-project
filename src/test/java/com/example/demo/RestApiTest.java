package com.example.demo;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.example.dto.BatchDto;
import com.example.dto.StudentDto;
import com.example.entity.Batch;
import com.example.restapi.BatchRestApi;
import com.example.service.BatchService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(BatchRestApi.class)
public class RestApiTest {

	@MockBean
	BatchService service;

	@Autowired
	MockMvc mvc;

	BatchDto batchDto = new BatchDto();
	Batch batch = new Batch();
	List<Batch> BatchList = new ArrayList<>();
	List<BatchDto> BatchDtoList;
	List<StudentDto> studentDtoList;
	StudentDto studentDto = new StudentDto();
	BatchDto dummyBatch=new BatchDto();

	@BeforeEach
	void setUp() {
		studentDtoList = new ArrayList<>();
		studentDtoList.add(studentDto);
		studentDto.setName("sreeja");
		batchDto.setName("java");
		batchDto.setStudent(studentDtoList);
		BatchDtoList = new ArrayList<>();
		BatchDtoList.add(batchDto);
	}

	@Test
	void addBatch() throws Exception {
		Mockito.when(service.addBatch(batchDto)).thenReturn(batchDto);
		mvc.perform(post("/batch").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(batchDto))).andExpect(status().isCreated()).andReturn();
	}

	@Test
	void getAllBatches() throws Exception {
		Mockito.when(service.getAllBatches()).thenReturn(BatchDtoList);
		mvc.perform(get("/batch")).andExpect(status().isOk()).andReturn();
	}

	@Test
	void getBatchById() throws Exception {
		Mockito.when(service.getBatchById(1)).thenReturn(batchDto);
		mvc.perform(get("/batch/{id}", 1)).andExpect(status().isOk()).andReturn();
	}

	@Test
	void deleteBatch() throws Exception {
		Mockito.doNothing().when(service).deleteBatch(1);
		mvc.perform(delete("/batch/{id}", 1)).andExpect(status().isNoContent());
	}

	 @Test
	    void testMethodNotValid() throws JsonProcessingException, Exception {
	    	Mockito.when(service.addBatch(dummyBatch)).thenReturn(dummyBatch);
	    	mvc.perform(post("/batch").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(dummyBatch)))
	    	.andExpect(status().isBadRequest())
	    	.andReturn();
	    	
	    }
	    @Test
	    void testMethodArgMismatch() throws Exception {
	    	Mockito.when(service.getBatchById(1)).thenReturn(batchDto);
	    	mvc.perform(get("/batch/Sreeja")).andExpect(status().isBadRequest()).andReturn();
	    }
	    @Test
	    void testhttpNotReadble() throws JsonProcessingException, Exception {
	    	Mockito.when(service.addBatch(dummyBatch)).thenReturn(dummyBatch);
	    	mvc.perform(post("/batch").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString("{name:")))
	    	.andExpect(status().isBadRequest())
	    	.andReturn();
	    }
	

}
