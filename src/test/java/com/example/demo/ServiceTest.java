package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.example.Demo1Application;
import com.example.dto.BatchDto;
import com.example.dto.StudentDto;
import com.example.entity.Batch;
import com.example.entity.Student;
import com.example.exception.BatchException;
import com.example.repository.BatchRepository;
import com.example.service.BatchService;

@ExtendWith(MockitoExtension.class)
class ServiceTest {

	@Mock
	BatchRepository batchRepository;
	@Mock
	ModelMapper mapper;
	@InjectMocks
	BatchService service;
	
	
	BatchDto batchDto=new BatchDto();
	Batch batch=new Batch();
	
	List<Batch> BatchList=new ArrayList<>();
	List<BatchDto> BatchDtoList=new ArrayList<>();
	
	List<StudentDto> studentDtoList=new ArrayList<>();
	List<Student> studentList=new ArrayList<>();
	
	StudentDto studentDto=new StudentDto();
	Student student=new Student();
	
	@BeforeEach
	void setUp()
	{
		studentList.add(student);
		student.setName("sreeja");
		batch.setName("java");
		batch.setStudent(studentList);
		BatchList.add(batch);
		
		studentDtoList.add(studentDto);
		studentDto.setName("sreeja");
		batchDto.setName("java");
		batchDto.setStudent(studentDtoList);
		BatchDtoList.add(batchDto);
	}
	
	@Test
	void addBatchTest()
	{
		Mockito.when(mapper.map(batchDto, Batch.class)).thenReturn(batch);
		Mockito.when(batchRepository.save(any())).thenReturn(batch);
		Mockito.when(mapper.map(batch, BatchDto.class)).thenReturn(batchDto);
		BatchDto savedBatch=service.addBatch(batchDto);
		assertEquals(savedBatch, batchDto);
		assertNotNull(batch.getName());
		assertNotNull(batch.getStudent());
		assertNotNull(batch.getId());
		assertNotNull(student.getId());
		assertNotNull(student.getName());
		assertNotNull(student.getBatch());
		Mockito.verify(mapper).map(batchDto, Batch.class);
		Mockito.verify(batchRepository).save(any());
		Mockito.verify(mapper).map(batch, BatchDto.class);
	}
	@Test
	void getAllBatches()
	{
		
		Mockito.when(batchRepository.findAll()).thenReturn(List.of(batch));
		Mockito.when(mapper.map(batch, BatchDto.class)).thenReturn(batchDto);
		List<BatchDto> BatchDtoList=service.getAllBatches();
		assertNotNull(BatchDtoList);
	}
	@Test
	void getBatchById()
	{
		batch.setId(1);
		student.setId(1);
		Mockito.when(batchRepository.findById(batch.getId())).thenReturn(Optional.of(batch));
		Mockito.when(mapper.map(batch, BatchDto.class)).thenReturn(batchDto);
		BatchDto batch=service.getBatchById(1);
		assertNotNull(batch);
	}
	@Test
	void getBatchIdnotPresent()
	{
		Mockito.when(batchRepository.findById(1)).thenReturn(Optional.empty());
		assertThrows(BatchException.class, ()-> service.getBatchById(1));
		
	}

	@Test
	void delete()
	{
		Mockito.doNothing().when(batchRepository).deleteById(1);
		service.deleteBatch(1);
		Mockito.verify(batchRepository).deleteById(1);
		
	}
		
	@Test
	   void applicationStarts() {
		Demo1Application.main(new String[] {});
	  }
	
	
	
	
	
	
	
	
	
}
