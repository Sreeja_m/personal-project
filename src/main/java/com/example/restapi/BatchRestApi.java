package com.example.restapi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.dto.BatchDto;
import com.example.service.BatchService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/batch")
public class BatchRestApi {

	@Autowired
	BatchService batchService;

	@PostMapping
	ResponseEntity<BatchDto> addBatch(@Valid @RequestBody BatchDto batchDto) {
		return new ResponseEntity<>(batchService.addBatch(batchDto), HttpStatus.CREATED);
	}

	@GetMapping
	ResponseEntity<List<BatchDto>> getAllBatch() {
		return new ResponseEntity<>(batchService.getAllBatches(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	ResponseEntity<BatchDto> getBatch(@PathVariable int id) {
		return new ResponseEntity<>(batchService.getBatchById(id), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	ResponseEntity<BatchDto> deleteBatch(@PathVariable int id) {
		batchService.deleteBatch(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@PutMapping("/{id}")
	public ResponseEntity<BatchDto> updateBatch(@PathVariable int id, @Valid @RequestBody BatchDto batchDto) {
		return new ResponseEntity<>(batchService.updateBatch(id, batchDto), HttpStatus.OK);
	}

}
