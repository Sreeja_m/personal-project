package com.example.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@NoArgsConstructor
public class BatchDto {
	private int id;
	@NotBlank
	private String name;
	@Size(min = 1,message = "each batch should contain atleast one student")
	@Valid
	List<StudentDto> student=new ArrayList<>();
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BatchDto other = (BatchDto) obj;
		return id == other.id && Objects.equals(name, other.name) && Objects.equals(student, other.student);
	}
	
}
