package com.example.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dto.BatchDto;
import com.example.entity.Batch;
import com.example.exception.BatchException;
import com.example.repository.BatchRepository;

@Service
public class BatchService {

	@Autowired
	BatchRepository batchRepository;
	@Autowired
	ModelMapper mapper;

	public BatchDto addBatch(BatchDto batchDto) {
		Batch savedBatch = batchRepository.save(mapper.map(batchDto, Batch.class));
		return mapper.map(savedBatch, BatchDto.class);
	}

	public List<BatchDto> getAllBatches() {
		return batchRepository.findAll().stream().map(i -> mapper.map(i, BatchDto.class)).toList();
	}

	public BatchDto getBatchById(int id) {
		return batchRepository.findById(id).map(i -> {
			return mapper.map(i, BatchDto.class);
		}).orElseThrow(() -> new BatchException("id not found"));
	}

	
	public BatchDto updateBatch(int id, BatchDto batchDto) {
		return batchRepository.findById(id).map(batch -> {
			Batch updatedBatch = batchRepository.save(mapper.map(batchDto, Batch.class));
			return mapper.map(updatedBatch, BatchDto.class);
		}).orElseThrow(() -> new BatchException("id doesnt exist"));
	}

	public void deleteBatch(int id) {
		batchRepository.deleteById(id);
	}

}
