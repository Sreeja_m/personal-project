package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.entity.Batch;

public interface BatchRepository extends JpaRepository<Batch, Integer> {

}
