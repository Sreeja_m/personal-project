package com.example.exception;

public class BatchException extends RuntimeException {
	public BatchException(String msg)
	{
		super(msg);
	}
}
