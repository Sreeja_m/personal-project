package com.example.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
public class ErrorResponse {
	private String timeStamp;
	private String status;
	private String error;
	private String path;
}
