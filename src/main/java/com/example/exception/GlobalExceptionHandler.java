package com.example.exception;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpServerErrorException.InternalServerError;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@RestControllerAdvice
public class GlobalExceptionHandler {

	
	
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ErrorResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException e, WebRequest w) {
		List<String> error = new ArrayList<>();
		e.getAllErrors().stream().forEach(err -> error.add(err.getDefaultMessage()));
		return new ErrorResponse(new Date().toString(), HttpStatus.BAD_REQUEST.toString(), error.toString(),
				w.getDescription(false));
	}
	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ErrorResponse handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e, WebRequest w) {
		return new ErrorResponse(new Date().toString(), HttpStatus.BAD_REQUEST.toString(), e.getMessage(),
				w.getDescription(false));
	}
	@ExceptionHandler(HttpMessageNotReadableException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ErrorResponse handleHttpMessageNotReadableException(HttpMessageNotReadableException e, WebRequest w) {
		return new ErrorResponse(new Date().toString(), HttpStatus.BAD_REQUEST.toString(), e.getMessage(),
				w.getDescription(false));
	}
	@ExceptionHandler(BatchException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ErrorResponse handleBatchException(BatchException e, WebRequest w) {
		return new ErrorResponse(new Date().toString(), HttpStatus.INTERNAL_SERVER_ERROR.toString(), e.getMessage(),
				w.getDescription(false));
	}
	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ErrorResponse handleRuntimeException(RuntimeException e, WebRequest w) {
		return new ErrorResponse(new Date().toString(), HttpStatus.INTERNAL_SERVER_ERROR.toString(), e.getMessage(),
				w.getDescription(false));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
